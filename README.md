# Helm Chart deps-chart and service-chart

```
helm upgrade --install deps-chart ./deps-chart

helm upgrade --install jobber ./service-chart -f jobber-values.yml
helm upgrade --install leads ./service-chart -f leads-values.yml
helm upgrade --install notif ./service-chart -f notif-values.yml
helm upgrade --install quiz ./service-chart -f quiz-values.yml
helm upgrade --install show ./service-chart -f show-values.yml
helm upgrade --install uploader ./service-chart -f uploader-values.yml
helm upgrade --install users ./service-chart -f users-values.yml
helm upgrade --install app ./service-chart -f app-values.yml
helm upgrade --install land ./service-chart -f land-values.yml
```